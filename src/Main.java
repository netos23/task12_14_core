import java.util.function.Consumer;

public class Main {

	public static void main(String[] args) {
		int k = 3;
		int[] els = {1, 2, 3, 4, 5};
		CombinationConsumer consumer = new CombinationConsumer();

		CombinationProvider.genAll(els, k, consumer);

		for(int[] ints : consumer.getCombinations()){
			for(int i : ints){
				System.out.print(i+" ");
			}
			System.out.println();
		}
	}
}
