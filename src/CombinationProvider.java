import java.util.LinkedList;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Collectors;

public class CombinationProvider {

	private static List<Integer> combination = new LinkedList<>();


	public static void genAll(int[] elements, int k,
							  Consumer<int[]> callback) {
		setup();
		genAll(elements, k, callback, 0);
	}

	private static void genAll(int[] elements, int k,
							   Consumer<int[]> callback, int index) {

		if (index == elements.length) {
			if (combination.size() == k) {
				int[] currentCombination = toArray();
				callback.accept(currentCombination);
			}
		} else {
			combination.add(elements[index]);
			// включить в подмножество

			genAll(elements, k, callback, index + 1);

			// не включить
			combination.remove(combination.size() - 1);
			genAll(elements, k, callback, index + 1);
		}
	}

	private static void setup() {
		combination.clear();
	}

	private static int[] toArray() {
		return combination.stream()
				.mapToInt(v -> v)
				.toArray();
	}


}
