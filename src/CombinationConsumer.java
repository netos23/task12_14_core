import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

public class CombinationConsumer implements Consumer<int[]> {
	private List<int[]> combinations;

	public CombinationConsumer() {
		combinations = new ArrayList<>();
	}

	@Override
	public void accept(int[] ints) {
		combinations.add(ints);
	}

	public List<int[]> getCombinations() {
		return combinations;
	}
}
